

var gridX = 10;
var gridY = 10;
var difficulty = 0.2;

var grid; // 0-8 pour nombres, 10 pour bombe
var gridBoolReveal; //0 false, 1 true, 2 pointeur

var nbBomb = 0, nbCasesLibres, nbPointeurs;
var play;




var canvas = document.getElementById("canvas");
var width = canvas.width;
var height = canvas.height;


var ctx = canvas.getContext("2d");


canvas.addEventListener('click', function(event) {
	var Xpressed = Math.trunc((event.clientX - 10) * gridX / width) ;
	var Ypressed = Math.trunc((event.clientY - 10) * gridY / height);
	//document.write(Xpressed+"<br>"); document.write(Ypressed+"<br>");
	reveal(Xpressed, Ypressed); 
	draw_background_grid();
}, false);


/*
function clickOn(event){
	var x = event.clientX;
    var y = event.clientY;
	document.write(x+"<br>");
	document.write(y+"<br>");
	//reveal(x, y); 
	//draw_background_grid();
}
*/




function init_grid(line, colomn, diff){
	canvas = document.getElementById("canvas");
	width = canvas.width;
	height = canvas.height;
	ctx = canvas.getContext("2d");
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	gridX = colomn;
	grixY = line;
	//init the grid with 0 & bomb=10
	grid = new Array(grixY);
	gridBoolReveal = new Array(grixY);
	for (var y = 0; y < grixY; y++){
		grid[y] = new Array(gridX);
		gridBoolReveal[y] = new Array(gridX);
		for (var x = 0; x < gridX; x++){
			gridBoolReveal[y][x] = 0;
			if (Math.random() < diff){
				grid[y][x] = 10;
				nbBomb ++;
			}else{
				grid [y][x] = 0;
			}
		}
	}
	nbCasesLibres = gridX * gridY - nbBomb;
	//add number arround
	for (var y = 0; y < grixY; y++){
		for (var x = 0; x < gridX; x++){
			if (grid[y][x] != 10){
				var countBombs = 0;
				for (var i = -1; i <= 1; i++) {
					for (var j = -1; j <= 1; j++) {
						if (i == 0 && j == 0) {
							continue;
						}
						if (x + j < 0 || x + j >= gridX || y + i < 0 || y + i >= gridY){
							continue;
						}
						var xx = ((x + j) + gridX) % gridX;
						var yy = ((y + i) + gridY) % gridY;
						if (grid[yy][xx] == 10){
							countBombs++;
						}
					}
				}
				grid[y][x] = countBombs;
			}
		}
	}
	draw_background_grid();
}

function reveal_all() {
	for (var y = 0; y < gridY; y++) {
		for (var x = 0; x < gridY; x++) {
			gridBoolReveal[y][x] = 1;
            draw_box(x, y);
        }
    }
}

function reveal_all_bomb() {
	for (var y = 0; y < gridY; y++) {
		for (var x = 0; x < gridY; x++) {
			if (grid[y][x] == 10) { 
			    gridBoolReveal[y][x] = 1;
				draw_box(x, y);
			}
		}
	}
}

function reveal(x, y) {
	gridBoolReveal[y][x] = 1;
	nbCasesLibres --;
    draw_box(x, y);
    if (grid[y][x] == 0) {
		for (var i = -1; i <= 1; i++) {
			for (var j = -1; j <= 1; j++) {
				if (i == 0 && j == 0) {
					continue;
				}
				if (x + j < 0 || x + j >= gridX || y + i < 0 || y + i >= gridX){
					continue;
				}
				var xx = ((x + j) + gridX) % gridX;
				var yy = ((y + i) + gridY) % gridY;
				if (gridBoolReveal[yy][xx] != 1){
					reveal(xx, yy);
				}
			}
		}
    }
}




function draw_background_grid() {
	for (var y = 0; y <= gridY; y++){
		ctx.moveTo(0, y * height / gridY);
		ctx.lineTo(width, y * height / gridY);
		ctx.stroke();
		for (var x = 0; x <= gridX; x++){
			ctx.moveTo(x * width / gridX, 0);
			ctx.lineTo(x * width / gridX, height);
			ctx.stroke();
		}
	}
}

function draw_box (x, y){
	ctx.font = '40px serif';
	ctx.textAlign = 'center';
	if (gridBoolReveal[y][x] == 2){
		ctx.fillText("P", x * width / gridX + width / (2*gridX), y * height / gridY + height / (2*gridY) + 10);
	} else if (gridBoolReveal[y][x] == 1){
		if (grid[y][x] == 10){
			ctx.fillStyle = "#ff0000";
			ctx.fillText("B", x * width / gridX + width / (2*gridX), y * height / gridY + height / (2*gridY) + 10);
			ctx.fillStyle = "#000000";
		} else if (grid[y][x] == 0){
			ctx.fillStyle = "#0000ff";
			ctx.fillText(grid[y][x], x * width / gridX + width / (2*gridX), y * height / gridY + height / (2*gridY) + 10);
			ctx.fillStyle = "#000000";
		} else {
			ctx.fillText(grid[y][x], x * width / gridX + width / (2*gridX), y * height / gridY + height / (2*gridY) + 10);
		}
	}
	
}


















init_grid(10, 10, 0.2);

//reveal_all(); //debug graphique grille
//reveal(7, 7); //test reveal

/*
//affichage de la grille pour debug
for (var i = 0; i < gridY; i++) { 
	document.write("<br>"); 
    for (var j = 0; j < gridX; j++)    { 
        document.write(grid[i][j] + " "); 
    } 
}
*/

