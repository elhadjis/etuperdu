#! /usr/bin/python
# -*- coding:utf-8 -*-
#
from flask import Flask, render_template, request, redirect
from flask_sqlalchemy import SQLAlchemy

def reponse(choix_utilisateur):
    #tableau de réponse
    reponses = [["Présentation du campus :","L’Université Grenoble Alpes représente un acteur majeur de l’enseignement supérieur et de la recherche en France. Dans un monde de plus en plus compétitif, notre établissement a pour ambition de mieux répondre à l’ensemble des défis posés aux universités par le monde d’aujourd’hui et de demain, et d’être encore plus visible et attractif à l’international.","Pour plus d'information : https://www.univ-grenoble-alpes.fr/"],
      ["Présentation et fonctionnement de la bourse universitaire","Si vos parents ont des ressources financières limitées, vous avez peut-être droit à une bourse sur critères sociaux pour vous aider à financer vos études dans une formation supérieure","Pour plus d'informations :http://www.crous-grenoble.fr/"],
      ["Faire une demande de bourse","En fonction de vos critères sociaux (revenus annuel de vos parents,distance des études,nombre d'enfant à charge, nombre d'enfant à charge dans l'enseignement supérieur), le crous est disposé à fournir une aide sociale. Pour constituer votre demande de bourse, vous devez alors créer un dossier social étudiant","Pour créer votre dossier :https://www.messervices.etudiant.gouv.fr/"],
      ["Connaître les montants des bourses attribuées:","En fonction de vos critères sociaux, le crous est disposé à donner une somme correspondant à un échelon:"],
      ["Faire une simulation de bourse:","LIENS VERS LE SIMULATEUR BOURSE CROUS"],
      ["Je n'ai pas reçu ma bourse","Le premier versement de votre bourse doit normalement avoir lieu le 5 septembre au plus tard. Mais pour cela, il faut faire partie des bons élèves. C’est-à-dire, avoir rempli et envoyé votre DSE (dossier social étudiant) complet (avec toutes les pièces justificatives) au CROUS avant le 15 mai 2019 et avoir réalisé votre inscription administrative auprès de votre établissement avant le 25 août. Au-delà, il peut y avoir un léger retard.Sachez d’ailleurs que si les CROUS se sont engagés à verser les bourses le 5 de chaque mois, il s’agit simplement d’une mise en paiement. Cela signifie que l’opération peut mettre plusieurs jours, -entre cinq et dix jours ouvrés-, avant d’atterrir sur votre compte bancaire. Tout dépend de votre banque.","Pour plus d'information sur votre dossier social étudiant :https://www.messervices.etudiant.gouv.fr/"],
      ["Faire une demande de logement étudiant:","Dans le cadre de vos études, le crous disposes de résidences qui sont attribués avec le dossier social étudiant.","Pour constituer votre dossier social étudiant: https://www.messervices.etudiant.gouv.fr/"],
      ["Présentation et fonctionnement des APL :","L'aide personnalisée au logement (APL) est une aide financière destinée à réduire le montant de votre loyer, mensualité d'emprunt ou redevance (si vous résidez en foyer). Elle est versée en raison de la situation de votre logement et ce, quelle que soit votre situation familiale : célibataire, marié, avec ou sans personne à charge. Les conditions d'attribution diffèrent selon que vous êtes en location, que vous accédez à la propriété ou que vous résidez en foyer."],
      ["Faire une demande d'APL:","Pour constituer votre dossier : https://wwwd.caf.fr/MaintenanceNocturne.html"],
      ["Faire une simulation d'APL :","LIEN VERS LA SIMULATION"],
      ["Information sur la réduction sur les transports","En fonction de votre localisation, la région est disposé à proposer des tarifs adaptés aux étudiants.","Pour connaitre les tarifs : https://www.tag.fr/"],
      ["Faire une simulation d'aide aux transports:","LIEN VERS LA SIMULATION"],
      ["Je n'arrive pas à créer mon compte EtuPerdu","La création du compte peut-être laborieuse, mais nous vous invitons à regarder si tous les champs nécéssaires ont été correctement remplis","Si votre problème persiste ou que vous n'avez pas une information recquise, nous vous redirigeons vers la section 'contact' du notre FAQ"],
      ["Aide pour le fonctionnement du forum: ","Le forum est un lieu de partage et d'échange des étudiants utilisant EtuPerdu. Il est basé sur l'entraide.","Si vous ne réussissez pas à créer votre compte ou bien à poster un message, nous vous invitons vers la présentation du fonctionnement du forum : LIEN FORUM","Si le problème persiste,nous vous redirigeons vers la section 'contact' du notre FAQ"],
      ["Je ne retrouve pas mon mot de passe","Nous vous invitons à réinnitialisé votre mot de passe EtuPerdu : LIEN"],
      ["Je suis un étudiant étranger","Informations sur les bourses attribuées aux étudiants étrangers","Le ministère français des affaires étrangères attribue de nombreuses bourses aux étudiants étrangers. 25% de ces bourses sont financées directement par le ministère dans le cadre des programmes Eiffel (pour des études au niveau master ou doctorat) ou Major (destiné aux meilleurs bacheliers étrangers des lycées français de l’étranger). Le reste est pris en charge par les ambassades de France à l’étranger.Renseignez-vous directement auprès de l’Espace Campus France de votre pays pour connaître les programmes de bourses qui vous concernent."],
      ["Contact","Vous pouvez entrer en contact avec :","L'équipe EtuPerdu","Le crous","Les services de la CAF","Les services de transport grenoblois TAG","sur leurs différents sites respectifs"]]
    
    liste_des_reponses = []
    for i in range(0,len(choix_utilisateur)):
        for j in range(0,len(reponses[choix_utilisateur[i]])):
            liste_des_reponses.append(reponses[choix_utilisateur[i]][j])
        liste_des_reponses.append(chr(0xa0))
    print(liste_des_reponses)
    return liste_des_reponses

app = Flask(__name__)
app.config['SQLALCHEMY_BINDS'] = {
    'user' :             'sqlite:///user.db'
}
db = SQLAlchemy(app)

class User(db.Model):
    __bind_key__ = 'user'
    id = db.Column(db.Integer, primary_key=True,autoincrement=True )
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(120))
    nom = db.Column(db.String(120), index=True, unique=True)
    prenom = db.Column(db.String(120), index=False, unique=False)
    datedeNaissance = db.Column(db.String(120), nullable=False)
    ville = db.Column(db.String(120), index=False, unique=False, nullable=False)
    ecole = db.Column(db.String(120), index=False, unique=False, nullable=False)
    annee = db.Column(db.Integer, index=False, unique=False, nullable=True)

    def __repr__(self):
        return '<User %r>' % self.username

    def __init__(self, email,nom,prenom,password,pseudo,date,ville,ecole,annee):
        self.email = email
        self.password_hash = password
        self.ecole=ecole
        self.annee=annee
        self.datedeNaissance=date
        self.ville=ville
        self.username=pseudo
        self.nom=nom
        self.prenom=prenom

    def login(self, email, password):
        if self.email == email and self.password_hash==password:
            return True
        else :
            return False
    def is_authenticated(self):
        return True
    def is_active(self):
        return True
    def is_anonymous(self):
        return False
    def get_id(self):
        return str(self.id)
		
@app.errorhandler(404)
def page_introuvable(error):
    return render_template('index.html'), 404

@app.route('/')
def homepage():
    return render_template('homepage.html')

@app.route('/login', methods=['POST','GET'])
def connexion():
    if request.method=='POST':
        print(request.form)
        pseudo = request.form['pseudo']
        email = request.form['email']
        password = request.form['pass']
        nom = request.form['nom']
        prenom = request.form['prenom']
        date = request.form['date']
        ville = request.form['ville']
        ecole = request.form['ecole']
        annee = request.form['annee']
        print('ok2')
        new_User = User(email=email,nom=nom,prenom=prenom,password=password,pseudo=pseudo,date=date,ville=ville,ecole=ecole,annee=annee)
        print(new_User)
        try:
            db.session.add(new_User)
            db.session.commit()
            return redirect('/')
        except Exception as e:
            raise e
            return 'On a trouvé une erreur dans vos données'
    else :
        return render_template('connexion.html')

@app.route('/simulation')
def simulation():
    return render_template('simulation_home.html')

@app.route('/about/')
def about():
    return render_template('about.html')

@app.route('/aides')
def aides():
    return render_template('aides.html')

@app.route('/forum')
def forum():
    return render_template('forum.html')

@app.route('/forum/1')
def f1():
    return render_template('f1.html')

@app.route('/forum/2')
def f2():
    return render_template('f2.html')

@app.route('/forum/3')
def f3():
    return render_template('f3.html')

@app.route('/forum/4')
def f4():
    return render_template('f4.html')

@app.route('/aides/bourses')
def aides_bourses():
    return render_template('aides_bourses.html')

@app.route('/aides/logement')
def aides_logement():
    return render_template('aides_logement.html')

@app.route('/aides/soins')
def aides_soins():
    return render_template('aides_soins.html')

@app.route('/aides/transports')
def aides_transports():
    return render_template('aides_transports.html')

@app.route('/actualites')
def actu():
    return render_template('actualites.html')

@app.route('/simulation_bourses', methods=['GET', 'POST'])
def simu_bourses():
    if request.method == 'POST':
        revenues = int(request.form.get('revenues'))
        distance = int(request.form.get('distance'))
        nb_fr_sr = int(request.form.get('nb_fr'))
        nb_fr_sup = int(request.form.get('nb_fr_sup'))
        bourse = crous(revenues, distance, nb_fr_sr, nb_fr_sup);
        return render_template('affichage_bourse.html',bourse=bourse)
    return render_template('simulation_bourses.html')

@app.route('/simulation_logement', methods=['GET', 'POST'])
def simu_():
    if request.method == 'POST':
        loyer = int(request.form.get('loyer'))
        revenus = int(request.form.get('revenus'))
        surface = int(request.form.get('surface'))
        code_postal = int(request.form.get('code_postal'))
        situation = (request.form.get('situation'))
        val = apl(loyer, revenus, surface, code_postal,situation);
        return render_template('affichage_logement.html',val=val)
    return render_template('simulation_logement.html')

@app.route('/simulation_transport', methods=['GET', 'POST'])
def simu_t():
    if request.method == 'POST':
        argent = int(request.form.get('revenues_tr'))
        bourse = int(request.form.get('bourses'))
        km = int(request.form.get('distance_ed'))
        val = transport(argent, bourse, km)
        return render_template('affichage_transport.html',val=val)
    return render_template('simulation_transport.html')

@app.route('/simulation_soins', methods=['GET', 'POST'])
def simu_s():
    if request.method == 'POST':
        revenus = int(request.form.get('revenus'))
        enfants = 0
        handicap = int(request.form.get('situation'))
        val=sante(revenus,enfants,handicap)
        return render_template('affichage_soins.html',val=val)
    return render_template('simulation_soins.html')

def crous(argent,km,nb_frere_soeur,nb_frere_sup):
    #renvoie le montant de la bourse a l'annee
    
    #Variable comptant le nombre de point de l'tudiant
    #Le montant de la bourse est donn� suivant le nombre de point
    point = 0
    if km > 30 and km < 249:
        point = point + 1
    if km > 250:
        point = point + 2
    point = point + nb_frere_sup*4
    point = point + (nb_frere_soeur - nb_frere_sup)*2
    
    #Calcul de la somme en fonction du nombre de point
    #On prend notre nombre de point, on cherche le revenue de l'ann�e N-2
    #�a nous donne un �chelon, qui m�ne � une somme sur l'ann�e
    #On a une matrice contenant les infos
    matrice_point_echelon = [[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0]]
    matrice_point_echelon[0] = [33100,22500,18190,16070,13990,11950,7540,250]
    matrice_construction_des_prix = [3472,2361,1900,1680,1450,1253,790,250]
    for i in range(0,8):
        for j in range(1,18):
            matrice_point_echelon[j][i] = matrice_construction_des_prix[i] * j + matrice_point_echelon[0][i]
    
    echelon = 0
    if point > 17:
        point = 17
    for i in range(0,8):
        if matrice_point_echelon[point][i] <= argent:
            echelon = i
            break
    argent_donner = [1020,1680,2500,3200,3900,4500,4800,5600]
    return argent_donner[echelon]     

def apl(loyer,revenus,surface,codepostal,situation):
    #renvoie le montant des apl par mois
    
    #situation : "locataire","sous-locataire"   
    #pas le vrai systeme mais pour simplifier je reprend le systeme de point du crous
    point = 0
    apl = 0
    if surface < 30:
        point = point +3
    if surface > 30 and surface < 70 :
        point = point +2
    if surface >70 and surface< 200:
        point = point +1
        
    if loyer < 500:
        point = point + 1
    if loyer>500 and loyer<800 :
        point = point+2
    if loyer>800 :
        point = point+3
        
    if revenus <= 10000:
        point = point + 3
    if revenus <= 20000 and revenus > 10000:
        point = point + 2
    if revenus < 40000 and revenus > 20000:
        point = point +1
    
    if situation == "locataire":
        point = point + 1
    if point <=14 and point >8:
        apl = 300
    if point <= 8 and point >3:
        apl = 200
    if point <= 3:
        apl = 100
    return apl
def transport(argent,bourse,km):
    #renvoie le pourcentage de reduction sur les titres de transports
    
    #argent = revenus annuel des parents sur une année fiscale
    #bourse = montant de la bourse par mois de l'etudiant
    #km = distance entre habitation et ecole, obligation de fournir un justificatif de domicile 
    if (argent < 10000) or (bourse > 300) or (km > 5):
        reduction = 40;
    if ((argent > 10000)and(argent<30000))or(bourse > 200) or ((km<5)and (km>2)):
        reduction = 30;
    if (argent > 30000)or(bourse > 300) or (km>1):
        reduction = 10;
    return reduction

def sante(revenus,enfant,handicap):
    #donne la part de la couverture santé pour l'étudiant
    
    #revenus= revnus annuel des parents sur une annee fiscale
    #enfants
    #handicap = aucun, leger (<80%), lourd (>80%)
    if (revenus < 10000) or (handicap == "Lourd"):
        return "Vous beneficiez d'une couverture complete des frais de sante"
    if ((revenus < 30000)and(revenus>=10000)) or (handicap == "Leger"):
        return "Vous beneficiez d'une couverture a 60% de vos frais de sante"
    else:
        return "Vous ne beneficiez pas de couverture en plus de la securite sociale"


@app.route('/FAQ', methods=['GET', 'POST'])
def FAQ():
    if request.method == 'POST':
        ls = [i for i in range(30) if str(i) in request.form]
        res = reponse(ls)
        print(res[0])
        return render_template('FAQ_reponse.html', val=res)
    return render_template('FAQ.html')

app.run(host='0.0.0.0', port=80, debug=True, threaded=True)
