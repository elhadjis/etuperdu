'''
Created on 5 déc. 2019

@author: Guillaume
'''


class Personne :
    '''
    Représentation objet d'une personne et de ses attributs :
    
    - Nom
    - Prenom
    - Pseudo
    - Age
    - Region
    - Ville
    - Ecole
    - Niveau etudes
    - Adresse mail
    - Telephone
    - Mot de passe
    
    '''
    
    
    

    def __init__(self):
        '''
        Constructor
        '''
        self.nom = ""
        self.prenom = ""
        self.pseudo = ""
        self.dateNaissance = ""
        self.ville = ""
        self.ecole = ""
        self.niveauEtudes = 0
        self.mail = ""
        self.telephone = ""
        self.motDePasse = ""
        
    
    def __init__arguments(self, nom, prenom, pseudo, dateNaissance, ville, ecole, niveauEtudes, mail, telephone, motDePasse):
        '''
        Constructor with arguments
        '''
        self.nom = nom
        self.prenom = prenom
        self.pseudo = pseudo
        self.dateNaissance = dateNaissance
        self.ville = ville
        self.ecole = ecole
        self.niveauEtudes = niveauEtudes
        self.mail = mail
        self.telephone = telephone
        self.motDePasse = motDePasse
    
    
    def _get_nom (self):
        return self.nom
    def _get_prenom (self):
        return self.prenom
    def _get_pseudo(self):
        return self.region
    def _get_dateNaissance(self):
        return self.dateNaissance
    def _get_ville(self):
        return self.ville
    def _get_ecole(self):
        return self.ecole
    def _get_niveauEtudes(self):
        return self.niveauEtudes
    def _get_mail(self):
        return self.mail
    def _get_telephone(self):
        return self.telephone
    def _get_motDePasse(self):
        return self.motDePasse
    
    
    def _set_nom (self):
        return self.nom
    def _set_prenom (self):
        return self.prenom
    def _set_pseudo(self):
        return self.region
    def _set_dateNaissance(self):
        return self.dateNaissance
    def _set_ville(self):
        return self.ville
    def _set_ecole(self):
        return self.ecole
    def _set_niveauEtudes(self):
        return self.niveauEtudes
    def _set_mail(self):
        return self.mail
    def _set_telephone(self):
        return self.telephone
    def _set_motDePasse(self):
        return self.motDePasse
    

    def compareTo(self, personne):
        return self.nom is personne.nom and self.prenom is personne.prenom \
            and self.mail is personne.mail and self.motDePasse is personne.motDePasse





















