'''
Created on 5 d�c. 2019

@author: Guillaume
'''
from array import array
from pycore.Personne import Personne

class Sujet_Forum:
    '''
    Representation d'un sujet de forum :
    
    - Titre
    - Auteur
    - Classe de sujet
    - Question principale
    - Liste de reponses
    
    
    '''


    def __init__(self):
        '''
        Constructor
        '''
        self.title = ""
        self.autor = Personne()
        self.subject = ""
        self.mainQuestion = ""
        self.listAnswer = array()


    def __init__arguments(self, title, autor, subject, mainQuestion, listAnswer):
        '''
        Constructor with arguments
        '''
        self.title = title
        self.autor = autor
        self.subject = subject
        self.mainQuestion = mainQuestion
        self.listAnswer = listAnswer

    def _get_title(self):
        return self.title
    def _get_autor(self):
        return self.autor
    def _get_subject(self):
        return self.subject
    def _get_mainQuestion(self):
        return self.mainQuestion
    def _get_listAnswer(self):
        return self.listAnswer


    def addAnswer(self, answer):
        self.listAnswer.append(answer)

    def removeAnswer (self, answer):
        for i in range(0, len(self.listAnswer)):
            if self.listAnswer[i].compareTo(answer):
                del self.listAnswer[i]
                return True
        return False









